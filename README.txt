This module is a greenhouse for Rules functionality – triggers, actions and
conditions that could be included in the Rules module, but probably should be
tested out and voted on first. Feel free to add your own ideas, opinions,
examples and feature requests!

INSTALLATION
============

The usual. Download, enable. You will need the Rules module. (Surprise!)


RULES BONUS: CCK
================

This module currently holds four new actions and one new condition:
* Set a CCK field without validation. This gives you a plain old textfield to
  enter data into CCK fields. That means that you won't be limited to
  validations or form elements that the CCK widget provides. Please have a look
  at http://drupal.org/node/1144404 if you'd like this functionality in Rules
  core.
* Insert a value in a multiple-value field. This action adds a new value to a
  multiple-value field, if it wasn't already present.
* Remove a value from a multiple-value field. This action kind of does the
  opposite to the previously described action.
* Get a field value, bypassing Token. This action gets a field value, even if
  a node hasn't been saved to the database yet. This is basically only for the
  cases when you're working on the node presave event, which causes Token to
  provide outdated content.
* Field value is one of several. This condition checks if a field contains one
  of several provided values. This could theoretically be done with Rules core,
  but would require terrible nesting of and/or statements.


RULES BONUS: MISCELLANEOUS
==========================

This module currently holds three actions and two conditions:
* Verify path argument. Provides the option to check an eqality condition on one
  part of the path.
* Check number of results from a view. This condition loads a view and checks
  the number of results – condition is passed if it is at least as many results
  as you set. You can pass on arguments to the view.
* Load path argument. Loads a selected part of the argument into a string.
* Get a string with current time. Creates a string object with the current time,
  in a custom format and with the acting user's time zone settings.
* Use Views to load a node. This action executes a view of your choice,
  including arguments you send to it, and loads the first node returned by the
  view as a new Rules object. (Note that you might first want to check that the
  view actually has any result using the condition above.)


RULES BONUS: NUMBERS
====================

This module currently holds three new actions:
* Generate a random number. This gives a random integer in a set interval. Some
  validation checks on the interval is still missing.
* Sum up numbers. This provides a sum of a list of numbers you write. Tokens are
  allowed.
* Multiply numbers. Much like the action above, but multiplies instead of adds.


RULES BONUS: USER
=================

This module currently holds two new actions:
* Load the acting user. Guess what this action does. :-)
* Load session ID for active user. Provides the session ID as a number object,
  as defined by the Session API module. (Session API module is required.)
