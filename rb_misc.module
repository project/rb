<?php

/**
 * @file
 * Miscellaneous conditions and actions for Rules.
 */

/**
 * Implementation of hook_rules_condition_info().
 * @ingroup rules
 */
function rb_misc_rules_condition_info() {
  $conditions = array(
    'rb_misc_condition_arg' => array(
      'label' => t('Verify path argument'),
      'module' => 'Rules Bonus: Misc',
      'eval input' => array('arg_value'),
    ),
  );

  // Add conditions that depend on non-required modules.
  if (module_exists('views')) {
    $conditions['rb_misc_action_views_result_count'] = array(
      'label' => t('Check number of results from a view'),
      'eval input' => array('args'),
      'module' => 'Rules Bonus: Misc',
    );
  }

  return $conditions;
}

/**
 * Implementation of hook_rules_action_info().
 * @ingroup rules
 */
function rb_misc_rules_action_info() {
  // Add the actions relevant for required core modules.
  $actions = array(
    'rb_misc_action_arg' => array(
      'label' => t('Load path argument'),
      'new variables' => array(
        'argument' => array(
          'type' => 'string',
          'label' => t('Path argument'),
          'save' => TRUE,
        ),
      ),
      'module' => 'Rules Bonus: Misc',
    ),
    'rb_misc_action_get_time' => array(
      'label' => t('Get a string with current time'),
      'new variables' => array(
        'current_time' => array(
          'type' => 'string',
          'label' => t('Current time'),
          'save' => TRUE,
        ),
      ),
      'module' => 'Rules Bonus: Misc',
    ),
  );

  // Add actions that depend on non-required modules.
  if (module_exists('views')) {
    $actions['rb_misc_action_views_load_node'] = array(
      'label' => t('Use Views to load a node'),
      'new variables' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Views-provided node'),
          'save' => TRUE,
        ),
      ),
      'eval input' => array('args'),
      'module' => 'Rules Bonus: Misc',
    );
  }

  return $actions;
}

/**
 * Configuration form for 'rb_misc_condition_arg'.
 */
function rb_misc_condition_arg_form($settings, &$form) {
  $form['settings']['arg_part'] = array(
    '#type' => 'select',
    '#options' => range(0, 9),
    '#default_value' => $settings['arg_part'],
    '#title' => t('Part of path argument'),
    '#description' => t('The part of the path argument to verify. Note that
      numbering starts with zero.'),
  );
  $form['settings']['arg_value'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['arg_value'],
    '#title' => t('Path argument value'),
    '#description' => t('The value the path argument should have.'),
  );
}

/**
 * The 'rb_misc_condition_arg' condition.
 */
function rb_misc_condition_arg($settings) {
  return (arg($settings['arg_part']) == $settings['arg_value']);
}

/**
 * Configuration form for 'rb_misc_action_views_result_count'.
 */
function rb_misc_action_views_result_count_form($settings, &$form) {

  $selectable_displays = array();
  foreach(views_get_all_views() as $view_name => $view) {
    if ($view->base_table == 'node') {
      foreach ($view->display as $display_name => $display) {
        $selectable_displays[$view_name . '|' . $display_name] =
          $view_name . '|' . check_plain($display->display_title);
      }
    }
  }
  
  $form['settings']['view'] = array(
    '#type' => 'select',
    '#options' => $selectable_displays,
    '#default_value' => $settings['view'],
    '#title' => t('Select view and display'),
    '#description' => t('Select the view and the view display you want to use to
      load a node. The first node listed in the view will be loaded.'),
  );
  $form['settings']['args'] = array(
    '#type' => 'textarea',
    '#default_value' => $settings['args'],
    '#title' => t('View arguments'),
    '#description' => t('Add any arguments you want to send to the view, one per
      line. You may use tokens.'),
  );
  $form['settings']['minimum'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum number of results'),
    '#default_value' => $settings['minimum'],
    '#description' => t('This condition returns TRUE if the view has at least
      the given number of results.'),
  );
}

/**
 * The 'rb_misc_action_views_result_count' condition.
 */
function rb_misc_action_views_result_count($settings) {
  $views_settings = explode('|', $settings['view']);
  $view_name = $views_settings[0];
  $display_name = $views_settings[1];
  $view_arguments = explode("\r", $settings['args']);

  $view = views_get_view($view_name);
  $view->set_display($display_name);
  $view->set_arguments($view_arguments);
  $view->set_items_per_page(0);
  $view->execute();
  
  $results = $view->result;
  return (count($results) >= $settings['minimum']);
}



/**
 * Configuration form for 'rb_misc_action_arg'.
 */
function rb_misc_action_arg_form($settings, &$form) {
  $form['settings']['arg_part'] = array(
    '#type' => 'select',
    '#options' => range(0, 9),
    '#default_value' => $settings['arg_part'],
    '#title' => t('Part of path argument'),
    '#description' => t('The part of the path argument to load. Note that
      numbering starts with zero.'),
  );
}

/**
 * The 'rb_misc_action_arg' action.
 */
function rb_misc_action_arg($settings) {
  return array(
    'arg_part' => arg($settings['arg_part']),
  );
}

/**
 * Configuration form for 'rb_misc_action_get_time'.
 */
function rb_misc_action_get_time_form($settings, &$form) {
  $form['settings']['format'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['format'],
    '#title' => t('Time format'),
    '#description' => t('A format for the time, as defined by the @PHP-date
      function. Could be \'Y-m-d\'.', array('@PHP-date' =>
      l('PHP date', 'http://php.net/manual/en/function.date.php'))),
  );
}

/**
 * The 'rb_misc_action_get_time' action.
 */
function rb_misc_action_get_time($settings) {
  $time = format_date(time(), 'custom', $settings['format']);
  return array(
    'current_time' => $time,
  );
}

/**
 * Configuration form for 'rb_misc_action_views_load_node'.
 */
function rb_misc_action_views_load_node_form($settings, &$form) {

  $selectable_displays = array();
  foreach(views_get_all_views() as $view_name => $view) {
    if ($view->base_table == 'node') {
      foreach ($view->display as $display_name => $display) {
        $selectable_displays[$view_name . '|' . $display_name] =
          $view_name . '|' . check_plain($display->display_title);
      }
    }
  }
  
  $form['settings']['view'] = array(
    '#type' => 'select',
    '#options' => $selectable_displays,
    '#default_value' => $settings['view'],
    '#title' => t('Select view and display'),
    '#description' => t('Select the view and the view display you want to use to
      load a node. The first node listed in the view will be loaded.'),
  );
  $form['settings']['args'] = array(
    '#type' => 'textarea',
    '#default_value' => $settings['args'],
    '#title' => t('View arguments'),
    '#description' => t('Add any arguments you want to send to the view, one per
      line. You may use tokens.'),
  );
}

/**
 * The 'rb_misc_action_views_load_node' action.
 */
function rb_misc_action_views_load_node($settings) {
  $views_settings = explode('|', $settings['view']);
  $view_name = $views_settings[0];
  $display_name = $views_settings[1];
  $view_arguments = explode("\r", $settings['args']);

  $view = views_get_view($view_name);
  $view->set_display($display_name);
  $view->set_arguments($view_arguments);
  $view->set_items_per_page(1);
  $view->execute();
  
  $results = $view->result;
  $node = node_load($results[0]->nid);

  return array('node' => $node);
}
